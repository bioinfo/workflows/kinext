<div align="center">

  <img src="assets/kinext.png" height="500">
</div>

[![KINEXT Version](https://img.shields.io/badge/kinext%20version-v3.1.0-red?labelColor=000000)](https://www.nextflow.io/)
[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A520.04.1-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)
[![Run with with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)
[![SeBiMER Docker](https://img.shields.io/badge/docker%20build-SeBiMER-yellow?labelColor=000000)](https://hub.docker.com/u/sebimer)

## Introduction

KiNext is a FAIR pipeline that uses [Nextflow](https://www.nextflow.io/) as a workflow manager (Di Tommaso *et al.*, 2017).
The objective of this pipeline is to search, classify and determine the phylogeny of the protein kinases of any organism from its proteome (predicted amino acid sequences).

- The first step of this pipeline is to identify "conventional" eukaryotic protein kinases (ePKs) and "atypical" protein kinases (aPKs) using Hidden Markov Models for each class of protein kinases. Two HMMs were generated using amino acid sequences from the catalytic domains of either type of known protein kinases (ePK and aPK) indentified in *H. sapiens*, *D. melanogaster* and *C. elegans* ([Kinbase](http://kinase.com/web/current/kinbase/)). Kinase search is conducted with [HMMER 3.3.2](http://eddylab.org/software/hmmer/Userguide.pdf).

- The second step further classifies ePKs, the largest class of protein kinases, into 8 families based on sequence similarity between the catalytic domains:
<div align="center">

  <img src="./assets/epk_table_families.png" height="150">
</div>


[Brown et al.](https://www.worldscientific.com/doi/abs/10.1142/9789812702456_0031) found that better separation between class members (ePK and aPK) is possible by using sub-family HMMs rather than a single HMM for the whole family.
Thus, the subdivision of large families increases the recognition accuracy of HMMs. [Miranda-Saavedra and D. and Barton](https://www.compbio.dundee.ac.uk/kinomer/download.html) indicate that this library (`/data/hmm_models/all_hmm_librairies_convert.hmm`) is superior not only to BLAST but also to a general HMM of the protein kinase catalytic domain for database searches. [Miranda-Saavedra, D. and Barton, G.J, 2007](https://www.compbio.dundee.ac.uk/kinomer/download.html)) 

Therefore, we used this library, which has subdivised family HMMs, against the previously identified ePKs and aPKs to accurately classify these protein kinases into families. The classification is done by a script using [python 3](https://www.python.org/downloads/).

- Once ePKs have been classified, phylogenetic analysis is carried out in order to cluster protein kinases according to sequence similarity among the catalytic domains using [IQTREE](http://www.iqtree.org/). Then, clades can be manually colored according to ePK family.


<div align="center">
  <img src="./assets/pipeline_KINEXT.png" height="450">
</div>


## How to start the pipeline

1. **Install [`nextflow`](https://www.nextflow.io/docs/latest/getstarted.html#installation)**


2. **Install [`Singularity`](https://www.sylabs.io/guides/3.0/user-guide/) for full pipeline reproducibility.**

3. **Download the pipeline**

Clone the current gitlab repository in your working directory on DATARMOR
```bash
git clone https://gitlab.ifremer.fr/bioinfo/workflows/kinnext.git
cd kinext
```

4. **Donwload the singularity images**

These singularity images (**`XXX.sif`**) will allow you to run the KiNext necessary tools in an isolated, self-contained environment that contains all the dependencies and libraries necessary for it to function properly.
```bash
cd kinext
cd containers
cd /containers/kinext-1.0
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/kinext-1.0.sif
cd /containers/iqtree-2.0.6
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/iqtree2.sif
cd /containers/hmmer-3.2
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/hmmer3_latest.sif

```

5. **Test the pipeline on a minimal dataset with a single command**

Run the pipeline on the test dataset, and verify that it produces the expected output files.
```bash
nextflow run main.nf -profile test,<singularity>
```

6.  **Start running your own analysis!**
```bash
nextflow run main.nf -profile <singularity>,custom [-c <institute_config_file>]
```

See [usage docs](docs/usage.md) for a complete description of all of the options available when running the pipeline.

## Documentation

**To be updated if necessary according to your development!**

- The KINEXT workflow comes with documentation about **[how to run the pipeline](docs/usage.md)**, found in the `docs/` directory
- The KINEXT workflow also comes with documentation about **[what kind of output are generated](docs/output.md)**, found in the `docs/` directory

## License and Credits
Kinext is a workflow conceived and implemented by [SeBiMER](https://ifremer-bioinformatics.github.io/), the Bioinformatics Core Facility of [IFREMER](https://wwz.ifremer.fr/en/).
This workflow was developed by ELisabeth Hellec, a Master 2 trainee at SeBiMER, the supervision of Alexandre Cormier, bioinformatics engineer. KiNext will be presented at the [JOBIM](https://www.sfbi.fr/blog/article/jobim-2023) (Open Days in Biology, Computer Science and Mathematics) conference which will take place from Tuesday 27 to Friday 30 June 2023.

## Contributions and Support

For further information or help, don't hesitate to contact us: **sebimer@ifremer.fr**.

## References

- Di Tommaso, P., Chatzou, M., Floden, E. et al. Nextflow enables reproducible computational workflows. Nat Biotechnol 35, 316–319, 2017.
- Manning G, D. B. Whyte, R. Martinez, T. Hunter, and S. Sudarsanam. “The Protein Kinase Complement of the Human Genome.” Science 298, no. 5600: e0155435, December 6, 2002.
- Epelboin Yanouk, Laure Quintric, Eric Guévélou, Pierre Boudry, Vianney Pichereau, and Charlotte Corporeau. “The Kinome of Pacific Oyster *Crassostrea gigas*, Its Expression during Development and in Response to Environmental Factors.” PLOS ONE 11, no. 5: e0155435, mai 2016.
- Hellec E. Identification et étude comparative du kinome chez l’huître creuse *Crassostrea  gigas* et le ver marin *Sabellaria alveolata*. Master 1 thesis, Université de Rennes 1, 2022. 
- Miranda-Saavedra, D, and Barton, G.J. Classification and functional annotation of eukaryotic protein kinases. Proteins 68, 893-914. PubMed, 2007.
- Brown D, Krishnamurthy N, Dale JM, Christopher W, Sjolander K. Subfamily hmms in functional genomics. Pac Symp Biocomput 2005: 322–333, 2005.


-- (c) 2023 - SeBiMER, Ifremer
