# USER GUIDE

In order to have all the images needed for the kinext pipeline to run, you need to retrieve:
- **the kinext image**
- **the hmmer image**
- **the IQTREE2 image**

``` bash
cd kinext
cd containers
cd /containers/kinext-1.0
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/kinext-1.0.sif
cd /containers/iqtree-2.0.6
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/iqtree2.sif
cd /containers/hmmer-3.2
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/hmmer-v3.2.1dfsg-1-deb_cv1.sif
wget https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/KINEXT/hmmer3_latest.sif
```
