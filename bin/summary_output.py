#!/usr/bin/env python3

import csv
from argparse import ArgumentParser

def main(args):
    #filed names
    fields = ['Kinase ID', 'family', 'family score', 'sub-family', 'sub-family score', 'description'] 
    fields2 = ['total AGC', 'total CAMK', 'total CK1', 'total CMGC', 'total RGC', 'total STE', 'total TK', 'total TKL', 'total Alpha', 'total RIO', 'total PIKK', 'total no annotation', 'total kinases'] 

    # Count
    ## ePKs
    CK1 = 0
    AGC = 0
    CMGC = 0
    CAMK = 0
    TK = 0
    TKL = 0
    STE = 0
    RGC = 0

    ## aPKs
    RIO = 0
    ALPHA = 0
    PIKK = 0

    ## no annotation
    NO_ANNOTATION = 0

    ## Total protein kinase identified
    TOTAL = 0

    dico = []
    dico2 = []
    kinase = {}
    epkapk = {}

    with open(args.hmmfamily, "r") as hmmout:
        for ligne in hmmout:
            if ligne.startswith("#"):
                continue
            id = ligne.split()[0] # Else, we keep the id of the sequences identified by hmmsearch
            subfamily = ligne.split()[2].split('_')[0] # We keek the subfamily of this sequence
            evalue = float(ligne.split()[4]) # And we also keep the evalue associated to this hit
            if id not in kinase: # If this idd is not in the dico containing the kinases
                kinase[id] = {'subfamily': subfamily, 'evalue': evalue}  # We added it to this dico, and we associate to this sequence another dico containing its evalue and its subfamily
            
            elif kinase[id]['evalue'] > evalue: # If the idd is alreay in the dico, we compare the current evalue and the evalue in the dico
                kinase[id]['subfamily'] = subfamily  # We keep the smallest evalue, and if we have to, we change the sunfamily
                kinase[id]['evalue'] = evalue # and the evalue of the sequence
                true_evalue = kinase[id]['evalue']

    with open(args.hmmkinase, "r") as hmmout:
        for ligne in hmmout:
            if ligne.startswith("#"):
                continue
            iddd = ligne.split()[0] # Else, we keep the id of the sequences identified by hmmsearch
            subfamily = ligne.split()[2].split('_')[0] # We keek the subfamily of this sequence
            evalue = float(ligne.split()[4]) # And we also keep the evalue associated to this hit
            if iddd not in epkapk: # If this idd is not in the dico containing the kinases
                epkapk[iddd] = {'subfamily': subfamily, 'evalue': evalue}  # We added it to this dico, and we associate to this sequence another dico containing its evalue and its subfamily
            
            elif epkapk[iddd]['evalue'] > evalue: # If the idd is alreay in the dico, we compare the current evalue and the evalue in the dico
                epkapk[iddd]['subfamily'] = subfamily  # We keep the smallest evalue, and if we have to, we change the sunfamily
                epkapk[iddd]['evalue'] = evalue # and the evalue of the sequence
                true_evalue = epkapk[iddd]['evalue']

    with open(args.annotated_kinome, "r") as annotated_kinome:
            for line in annotated_kinome: # For each line in the annotated kinome
                if line.startswith(">"): # If the line stat with >
                    idd = line.split()[0] 
                    idd = idd.replace(">", "")
                    idd = idd.split("_")[:-1] # We keep evrything BEFORE the last "_"
                    idd = "_".join(idd)
                    family = line.split()[3] # We keek the subfamily of this sequence
                    #print(family)
                    subfamily = line.split()[0].split('_')[-1:] # We keep evrything AFTER the last "_"
                    subfamily = ".".join(subfamily)
                    #print(subfamily)
                    description = line.split('|')[2]
                    #print(description)
                    
                    dico.append({'Kinase ID': idd, "family": family, "family score": epkapk[idd]['evalue'], "sub-family": subfamily, "sub-family score": kinase[idd]['evalue'], "description" : description})
                    
                    with open("1_summary_table_output.csv", 'w', newline='') as file:
                        writer = csv.DictWriter(file, fieldnames = fields)
                        writer.writeheader() 
                        writer.writerows(dico)

    with open(args.whole_kinome, "r") as whole_kinome:
        for line in whole_kinome: # For each line in the whole kinome
            if line.startswith(">"): # If the line stat with >
                                    subfamily = line.split()[0].split('_')[-1:]
                                    subfamily = ".".join(subfamily)
                                    # Final count of the identified protein kinases
                                    if subfamily == "CK1":
                                        CK1 += 1
                                        TOTAL += 1
                                    elif subfamily == "AGC":
                                        AGC += 1
                                        TOTAL += 1
                                    elif subfamily == "CAMK":
                                        CAMK += 1
                                        TOTAL += 1
                                    elif subfamily == "CMGC":
                                        CMGC += 1
                                        TOTAL += 1
                                    elif subfamily == "STE":
                                        STE += 1
                                        TOTAL += 1
                                    elif subfamily == "TK":
                                        TK += 1
                                        TOTAL += 1
                                    elif subfamily == "TKL":
                                        TKL += 1
                                        TOTAL += 1
                                    elif subfamily == "RGC":
                                        RGC += 1
                                        TOTAL += 1
                                    elif subfamily == "Alpha":
                                        ALPHA += 1
                                        TOTAL += 1
                                    elif subfamily == "RIO":
                                        RIO += 1
                                        TOTAL += 1
                                    elif subfamily == "PIKK":
                                        PIKK += 1
                                        TOTAL += 1
                                    elif subfamily == "annotation":
                                        NO_ANNOTATION += 1
                                        TOTAL += 1


    # Dico for the second output file
    dico2.append({'total AGC': AGC, "total CAMK": CAMK, "total CK1": CK1, "total CMGC": CMGC, "total RGC": RGC, "total STE" : STE, "total TK" : TK, "total TKL" : TKL, "total Alpha" : ALPHA, "total RIO" : RIO, "total PIKK" : PIKK, "total no annotation" : NO_ANNOTATION, "total kinases" : TOTAL})

    with open("2_summary_table_output.csv", 'w', newline='') as file:
        writer = csv.DictWriter(file, fieldnames = fields2)
        writer.writeheader() 
        writer.writerows(dico2)

def get_args():
    parser = ArgumentParser(description="")
    parser.add_argument("-H1", "--hmmkinase", help="Hmmsearch output of the kinases identification", required=True)
    parser.add_argument("-H2", "--hmmfamily", help="Hmmsearch output of the kinases classification", required=True)
    parser.add_argument("-W", "--whole_kinome", help="Whole kinome in fasta format", required=True)
    parser.add_argument("-A", "--annotated_kinome", help="Annotated kinome in fasta format", required=True)

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)
