#!/usr/bin/env python3

from argparse import ArgumentParser
from loguru import logger
from Bio import SeqIO


def main(args):
    logger.start(f'Start analysis')
    kinase = get_sub_families(args.hmmout)
    out_fasta(args.kinome, kinase)


def get_sub_families(hmmout):
    kinase = {} # Dico containing all kinases
    with open(hmmout, "r") as hmmsearch: # Opening the hmm output
        for line in hmmsearch: # For each line in this output
            if line.startswith("#"): # If the line stat with #
                continue # We are not interrested
            idd = line.split()[0] # Else, we keep the id of the sequences identified by hmmsearch
            subfamily = line.split()[2].split('_')[0] # We keek the subfamily of this sequence
            evalue = float(line.split()[4]) # And we also keep the evalue associated to this hit

            if idd not in kinase: # If this idd is not in the dico containing the kinases
                kinase[idd] = {'subfamily': subfamily, 'evalue': evalue}  # We added it to this dico, and we associate to this sequence another dico containing its evalue and its subfamily
            elif kinase[idd]['evalue'] > evalue: # If the idd is alreay in the dico, we compare the current evalue and the evalue in the dico
                kinase[idd]['subfamily'] = subfamily  # We keep the smallest evalue, and if we have to, we change the sunfamily
                kinase[idd]['evalue'] = evalue # and the evalue of the sequence
    return kinase


def out_fasta(fasta, kinase):
    whole_kinome = open('whole_kinome.fasta', 'w') # whole KINOME output file
    kinome = open('kinome_annot.fasta', 'w') # KINOME annotated output file
    kinase_no_annot = open('kinase_no_annot.fasta', 'w') # Kinases without annotation file
    epk = open('epk_annot.fasta', 'w') # EPK annotated output file
    apk = open('apk_annot.fasta', 'w') # APK annotated output file

    file = open(fasta, "r")
    for seq in SeqIO.parse(file, "fasta"): # For each sequence in the proteome:
        if seq.id in kinase: # For each id in the dico find in 'get_sub_family()' previously:
            seq_id = seq.id 
            seq.id = seq.id + '_' + kinase[seq.id]['subfamily'] + ' | ' # We change the seq_id to add the subfamily
            SeqIO.write(seq, kinome, 'fasta') # We write the sequence in the kinome
            SeqIO.write(seq, whole_kinome, 'fasta') # We write the sequence in the complete kinome
            
            if kinase[seq_id]['subfamily'] in ['Alpha', 'RIO', 'PIKK']: # if the subfamily of the idd is an APK:
                SeqIO.write(seq, apk, 'fasta') # Then we added it to the APK file
            if kinase[seq_id]['subfamily'] in ['AGC', 'CAMK', 'CK1', 'CMGC', 'RGC', 'STE', 'TK', 'TKL']: # if the subfamily of the idd is an EPK:
                SeqIO.write(seq, epk, 'fasta') # Then we added it to the EPK file
        else: # If the seqence has been identified as a kinase, be hasen't been associated to a subfamily:
            seq.id = seq.id + '_no_annotation |' # We change the seq_id to precise it hasn't an annotation
            SeqIO.write(seq, kinase_no_annot, 'fasta') # Then we added it to the "no_annotation_" file
            SeqIO.write(seq, whole_kinome, 'fasta') # We write the sequence in the complete kinome
    
    whole_kinome.close()
    kinome.close()
    epk.close()
    apk.close()


def get_args():
    parser = ArgumentParser(description="")
    parser.add_argument("-hmm", "--hmmout", help="Hmmsearch output of the best hits against the library of all kinases subfamiles", required=True)
    parser.add_argument("-k", "--kinome", help="Proteome fasta file in order to keep the sequences", required=True)
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)
