#!/usr/bin/env python

from argparse import ArgumentParser
from loguru import logger
from Bio import SeqIO


def main(args):
    logger.start(f'Start analysis')
    kinase = get_family(args.hmmout)
    out_fasta(args.proteome, kinase)


def get_family(hmmout):
    kinase = {} # Creation of a dictionnary of the final finases identified by hmmsearch
    with open(hmmout, "r") as hmmsearch:# Opening the hmm search output
        for line in hmmsearch: # For line in this file
            if line.startswith("#"): # If the line starts with "#"
                continue # We pass the line
            logger.info(f'Start analysis') # Then we can identify the sequences found by hmm search
            idd = line.split()[0] # ID of the sequence
            family = line.split()[2] # Family of the sequence
            evalue = float(line.split()[4]) # Evalue of the sequence
            logger.info(f'Get the idd, the family and the evalue of the sequence')

            if idd not in kinase: # If the current idd is not in the dico containing the result
                kinase[idd] = {'family': family, 'evalue': evalue} # Then a dico of dico is created in order to associate the family and the evalue for the idd
            elif kinase[idd]['evalue'] > evalue: #  If the idd is in the dico, and if the current evalue is < than the evalue already in the dico
                kinase[idd]['family'] = family # The family is changed with the current one
                kinase[idd]['evalue'] = evalue # the evaule is changed with the current one
    logger.info(f'End of the trimming of the HMMsearch sequences')
    return kinase


def out_fasta(fasta, kinase):
    kinome = open(args.kinome, 'w') # KINOME output file
    epk = open('epk.fasta', 'w') #  EPK output file
    apk = open('apk.fasta', 'w') # APK output file

    proteome = open(fasta, "r")
    for seq in SeqIO.parse(proteome, "fasta"): # For each sequences in the proteome
        if seq.id in kinase: # If the sequence id is in the trimmed dico "kinase"
            seq.description = kinase[seq.id]['family'] + ' | ' + seq.description # Then we add the family to the description
            SeqIO.write(seq, kinome, 'fasta') # KINOME file created
            if kinase[seq.id]['family'] == 'apk_domain': # If the family is "apk"
                SeqIO.write(seq, apk, 'fasta') # Then the sequence is added to the apk file
            if kinase[seq.id]['family'] == 'epk_domain': # If the family is "epk"
                SeqIO.write(seq, epk, 'fasta') # Then the sequence is added to the epk file
    logger.info(f'Writting the epk, apk and kinome files')
    kinome.close()
    epk.close()
    apk.close()


def get_args():
    parser = ArgumentParser(
        description="This script has to be used in order to classify the sequences found by the epk/apk library.")
    parser.add_argument("-hmm", "--hmmout", help="file containing sequences find by hmmsearch", required=True)
    parser.add_argument("-p", "--proteome", help="Proteome fasta file in order to recup the sequences", required=True)
    parser.add_argument("-k", "--kinome", help="Name the kinome output", required=False, default="kinome.fasta")

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)
