#!/usr/bin/env python3
from ete3 import Tree, TreeStyle, NodeStyle, AttrFace
from argparse import ArgumentParser

def main(args):

    nstyle = NodeStyle()
    dicocolor = {}

    # init colors, note capitals: http://etetoolkit.org/docs/latest/reference/reference_treeview.html#color-names
    colors = {'RGC': 'green' ,'TK':'Red', 'TKL':'LightGreen', 'STE':'Aqua', 'CAMK':'purple', 'CMGC':'pink', 'CK1':'orange', 'AGC':'yellow', 'PIKK':'teal', 'Alpha':'violet'}

    # init a tree
    t = Tree(args.tree , quoted_node_names=True, format=1)

    # Found the coulor associated to the ID
    for l in t.iter_leaves():
        #print (l)
        for color in colors:
            if color in l.name:
                dicocolor[l.name] = colors[color]
    #print(dicocolor)

    ##################################################################################
    #################### COLORATION OF THE PHYLOGENETIC TREE #########################
    ##################################################################################

    for l in t.iter_leaves():
        for id in dicocolor:
            if id == l.name:
                NameAroundTheTree = AttrFace("name", fgcolor = "black", fsize = 100)
                l.add_face(NameAroundTheTree, 1, position ='aligned')
                img_style = nstyle 
                idd = t & id # if the id is in the tree
                idd.img_style["bgcolor"] = dicocolor[l.name] # color the area around the sequence

    ##################################################################################
    ###################### CONFIG THE PHYLOGENETIC TREE ##############################
    ##################################################################################

    ts = TreeStyle()
    #ts.title.add_face(TextFace("Automatic staining of the phylogenetic tree of protein kinases according to their family", fsize=300, column=0 ))
    ts.show_leaf_name = True
    ts.show_branch_length = True
    ts.show_branch_support = True
    ts.mode = "c"
    ts.arc_start = -360 # 0 degrees = 3 o'clock
    ts.arc_span = 360
    #t.show(tree_style = ts)

    ##################################################################################
    ####################### PHYLOGENETIC TREE OUTPUT #################################
    ##################################################################################

    # render image on notebook
    t.render("phylogenetic_tree.png", tree_style=ts)

def get_args():
    parser = ArgumentParser(description="")
    parser.add_argument("-t", "--tree", help="phylogenetic tree - Newick format", required=True)
    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    main(args)
