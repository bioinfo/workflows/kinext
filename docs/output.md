# KINEXT: OUTPUT
This document describes the output files produced by the KiNext pipeline.

## The output files

All KiNext pipeline results are stored in `results/[projectName]/02_results/`.

## Pipeline overview
The pipeline is built using [Nextflow](https://www.nextflow.io/) and processes data using the following steps:

### Identification of all kinases
- Search the proteome for ePKs and aPKs using a separate HMM model for each class of kinase. Two classes of protein kinases (ePK and aPK) are searched for in parallel using one HMM for ePK and another HMM for aPK. ([HMMsearch](http://hmmer.org/))
- Classification epk/apk - Classification of HMMsearch results ([Python](https://www.python.org/downloads/))
- Fasta file of the full kinome - Creation of fasta files containing all identified kinases ([Python](https://www.python.org/downloads/))

### Annotation of kinases into Families
- Kinases identified in the previous step are further categorised into one of 8 kinase families. An HMM model generated for each kinase family is used to search for homologous sequences in the ePK fasta file produced for the target organism. An intermediate results file associates the family with lowest e-value to each sequence. ([HMMsearch](http://hmmer.org/), [Python](https://www.python.org/downloads/))
- Annotated kinome fasta file - Creation of a fasta files for each kinase class (ePK and aPK) containing kinases that have been attributed to their family. ([Python](https://www.python.org/downloads/))
- Summary table - creation of a table (csv) summarising all the protein kinases identified and classified according to their family ([Python](https://www.python.org/downloads/))

### Phylogeny of ePK steps
- Alignment of the ePK kinome - The fasta file containing the annotated ePKs are aligned against the original ePK HMM model (based on human, mouse and fly) ([HMMalign](http://hmmer.org/))
- Phylogeny - Phylogenetic construction from the HMMalign output ([IQTREE](http://www.iqtree.org/))

### Phylogeny of aPK steps
- Alignment of the aPK file - The fasta file containing the annotated aPKs are aligned against the original aPK HMM model (based on human and mouse)to the HMM aPK model ([HMMalign](http://hmmer.org/))
- Phylogeny - Phylogenetic construction from the HMMalign output ([IQTREE](http://www.iqtree.org/))

## Identification of all kinases

### Search kinases (ePK/aPK)

**Step input:**
- Proteome (predicted amino acid sequences) in fasta format
- ePK and aPK HMMs profiles: **`epk_apk_model.hmm`**

The [HMMER](http://hmmer.org/) tool is used in this step to search for protein kinases within the proteome of the given species. This is a **bash script** that allows us to obtain an array of all the sequences identified as "ePK" or as "aPK".
A data array **`XXXX_protein_epk_apk_perseqhit.out`** is produced in the pipeline output directory:
**`results/[projectName]/02_results/unsorted/hmmsearch/`** :

<div align="center">
  <img src="images/hmmsearch_apk_epk.png" height="200">
</div>

### Kinases classification ePK/aPK

**Step input:**
- Proteome (predicted amino acid sequences) in fasta format
- HMMsearch output

Python is used for this step to sort the hits identified by HMMsearch for ePK and the aPK HMMs.

**The output of this step is several fastas files:**
- A fasta file containing ePK search results: **`XXX_protein_epk.fasta`**
- A fasta file containing aPK search results: **`XXX_protein_apk.fasta`**
- A fasta file with all identified protein kinases (ePK + aPK): **`XXX_protein_kinome.fasta`**

All fasta files produced in the pipeline are saved in an output directory:
**`results/[projectName]/02_results/assigned_epk_apk/`**


Example of output files for *Crassostrea gigas*'s proteome:
<div align="left">
  <img src="images/assigned_epk_apk_output.png" height="50">
</div>

_________________________________________________________________________________________________________________________

**Here is the complete part of the pipeline concerning the kinase identification:**
<div align="center">
  <img src="images/classification_epk_apk.png" height="500">
</div>

_________________________________________________________________________________________________________________________


## Annotaton of kinases into families

### Search kinases families 

**Step input:**
- Kinome in fasta format
- HMM models of the protein kinase families: **`all_hmm_libraries_convert.hmm`**

The [HMMER](http://hmmer.org/) tool is used in this step to search for each protein kinase family. This is a **bash script** that allows us to obtain an array of all the sequences associated to their families.
A data array **`XXXX_protein_kinome_families_perseqhit.out`** is produced in the pipeline output directory :
**`results/[projectName]/02_results/hmmsearch/`** :

<div align="center">
  <img src="images/hmmsearch_families.png" height="200">
</div>

### Annotation into families

**Step input:**
- Kinome in fasta format
- Proteome (predicted amino acid sequences) in fasta format
- HMMsearch output

Python is used for this step to sort the hits identified by HMMsearch for each of the HMM models in Library 2. Each family is split into several HMM models to improve the identification of protein kinases. This **Python script** allows to keep the best result for the sequence identified several times. To achieve this, this script will search for the lowest e-value if the sequence has been found several times during the HMMsearch step.

**The output of this step is several fastas files:**
- A fasta file for the annotated epk: **`XXX_protein_annotated_epk.fasta`**
- A fasta file for the annotated apk: **`XXX_protein_annotated_apk.fasta`**
- A fasta file for the annotated kinome: **`XXX_protein_annotated_kinome.fasta`**
- A fasta file for the protein kinases which have been identified as kinases, but haven't been classified: **`XXX_protein_kinome_no_annot.fasta`**
- A fasta file containing the annotated kinome + the unclassified kinases: **`XXX_protein_whole_kinome.fasta`**


Those fasta files are produced in the pipeline output directory:
**`results/[projectName]/02_results/classify_families/`**

Example of output files for *Crassostrea gigas*'s annotated kinome:
<div align="left">
  <img src="images/classify_families_output.png" height="60">
</div>

_________________________________________________________________________________________________________________________

**Here is the complete part of the pipeline concerning the kinase classification into sub-families:**
<div align="center">
  <img src="images/classification_families.png" height="500">
</div>

_________________________________________________________________________________________________________________________

### Summary tables

**Step input:**
- Annotated kinome in fasta format
- Proteome (predicted amino acid sequences) in fasta format
- HMMsearch kinases output
- HMMsearch families output

This step allows the creation of 2 files in csv format which summarise all the protein kinases identified and to which family they belong: **`XXX_1_protein_summary_table_output.csv`** and **`XXX_2_protein_summary_table_output.csv`**. This step is performed using a ([Python](https://www.python.org/downloads/)) script.

Here is an example of **`XXX_1_protein_summary_table_output.csv`**:
<div align="center">
  <img src="images/summary_table.png" height="350">
</div>

Here is an example of **`XXX_2_protein_summary_table_output.csv`**:
<div align="center">
  <img src="images/2_table_output.png" height="80">
</div>

## Phylogeny of epk (eukaryotic protein kinases)

### Alignment of the epk fasta file

**Step input:**
- Proteome (predicted amino acid sequences) in fasta format
- epk fasta file: **`XXX_protein_annotated_epk.fasta`**
- epk HMMmodel: **`epk_model.hmm`**

Here, the file containing the annotated epk from the previous step is **aligned against the HMM epk model using HMMalign from the HMMER tool**. We obtain an aligned file in **phylip** format which will be reused later to build the phylogeny of epk protein kinases.

An aligned file **`XXXX_protein_epk_hmmalign.phy`** is produced in the pipeline output directory:
**`results/[projectName]/02_results/unsorted/hmmalign/`** :

<div align="center">
  <img src="images/hmmalign_epk_output.png" height="200">
</div>

:rotating_light: **WARNING, it is recommended to check the alignment quality of this file to be able to interpret your phylogeny later. You can check the quality of this alignment by looking at it with the [jalview tool](https://www.jalview.org/download/) for example.** :rotating_light:

### Construction of the epk phylogenetic tree

**Step input:**
- Proteome in fasta format
- HMMalign output file: **`XXX_protein_epk_hmmalign.phy`**

The phylogenetic tree is built with IQtree v2.0.3 (Minh et al. 2020) and its following options: "-st AA" indicating that these are amino acid sequences, "-bb 1000" to specify the use of the "ultrafast bootstrap" whose values become consequently less biased (95% that a clade is true) and which is recommended to be combined with the SH-aLRT test (Guindon et al. 2010) by adding "-alrt 1000", and finally the option "-m MFP" which allows the construction of the tree according to the best model identified by the tool itself, 2010) by adding "-alrt 1000", and finally the option "-m MFP" which allows the construction of the tree according to the best model identified by the tool itself.

Example of output files for *Crassostrea gigas*'s epk tree file:

<div align="center">
  <img src="images/iqtree_epk_output.png" height="200">
</div>


### Visualisation of the epk phylogenetic tree

This step is not included in the pipeline. The epk phylogenetic tree generated by [IQtree](http://www.iqtree.org/) can be visualised using [Figtree v1.4.4](http://tree.bio.ed.ac.uk/software/figtree/). 


_________________________________________________________________________________________________________________________

**Here is the complete part of the pipeline concerning the epk kinases phylogeny:**
<div align="center">
  <img src="images/epk_phylogeny.png" height="300">
</div>

_________________________________________________________________________________________________________________________


## Phylogeny of apk (atypical protein kinases)

### Alignment of the apk fasta file

**Step input:**
- Proteome in fasta format
- apk fasta file: **`XXX_protein_annotated_apk.fasta`**
- apk HMMmodel: **`apk_model.hmm`**

Here, the file containing the annotated apk from the previous step is **aligned against the HMM apk model using HMMalign from the HMMER tool**. We obtain an aligned file in **phylip** format which will be reused later to build the phylogeny of apk protein kinases.

An aligned file **`XXXX_protein_apk_hmmalign.phy`** is produced in the pipeline output directory:
**`results/[projectName]/02_results/unsorted/hmmalign/`** :

<div align="center">
  <img src="images/hmmalign_apk_output.png" height="200">
</div>

:rotating_light: **WARNING, it is recommended to check the alignment quality of this file to be able to interpret your phylogeny later. You can check the quality of this alignment by looking at it with the jalview tool for example.** :rotating_light:

### Construction of the apk phylogenetic tree

**Step input:**
- Proteome in fasta format
- HMMalign output file: **`XXX_protein_apk_hmmalign.phy`**

The phylogenetic tree is built with IQtree v2.0.3 (Minh et al. 2020) and its following options: "-st AA" indicating that these are amino acid sequences, "-bb 1000" to specify the use of the "ultrafast bootstrap" whose values become consequently less biased (95% that a clade is true) and which is recommended to be combined with the SH-aLRT test (Guindon et al. 2010) by adding "-alrt 1000", and finally the option "-m MFP" which allows the construction of the tree according to the best model identified by the tool itself, 2010) by adding "-alrt 1000", and finally the option "-m MFP" which allows the construction of the tree according to the best model identified by the tool itself.

Example of output files for *Crassostrea gigas*'s apk tree file:

<div align="center">
  <img src="images/iqtree_apk_output.png" height="50">
</div>

### Visualisation of the apk phylogenetic tree

This step is not included in the pipeline. The apk phylogenetic tree generated by [IQtree](http://www.iqtree.org/) can be visualised using [Figtree v1.4.4](http://tree.bio.ed.ac.uk/software/figtree/). 

_________________________________________________________________________________________________________________________

**Here is the complete part of the pipeline concerning the apk kinases phylogeny:**
<div align="center">
  <img src="images/apk_phylogeny_automatisation.png" height="250">
</div>

_________________________________________________________________________________________________________________________
