# KINEXT: Usage

# Table of contents
* [Introduction](#introduction)
* [How to install the pipeline](#install-the-pipeline)
* [How to run the pipeline](#running-the-pipeline)
  * [Updating the pipeline](#updating-the-pipeline)
  * [Reproducibility](#reproducibility)
* [arguments](#mandatory-arguments)
  * [`--profile`](#-profile)
* [Job resources](#job-resources)
  * [Automatic resubmission](#automatic-resubmission)
  * [Custom resource requests](#custom-resource-requests)
* [Optional command line parameters](#other-command-line-parameters)
  * [`--outdir`](#--outdir)
  * [`--work-dir`](#--work-dir)
  * [`--projectName`](#--projectName)
  * [`-name`](#-name)
  * [`-resume`](#-resume)

## Introduction

Nextflow handles job submissions on SLURM or other environments, and supervises running the jobs. Thus the Nextflow process must run until the pipeline is finished. We recommend that the process be run in the background through `screen` / `tmux` or similar tool. Alternatively Nextflow can run on a computing cluster submitted via a job scheduler.

It is recommended to limit the  memory allocated to the Nextflow Java virtual machines. We recommend adding the following line to your environment (typically in `~/.bashrc` or `~./bash_profile`) which limits memory allocation from 1 to 4 Gb:

```bash
NXF_OPTS='-Xms1g -Xmx4g'
```
## How to install the pipeline

### Local installation

Make sure that [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html) is installed on your system as well as [Singularity](https://docs.sylabs.io/guides/3.0/user-guide/installation.html). Working in the Nexflow environment, which allows  workflow management, ensures full reproducibility of the Kinext pipeline.

**How to install KiNext:**
```bash
git clone https://gitlab.ifremer.fr/bioinfo/workflows/kinnext.git
```

### Cluster configuration file
To use KiNext on a computing cluster, it is necessary to provide a Nextflow configuration file for your system. For some institutes, this may already exist and is referenced in nf-core/configs. If so, you can simply copy your institute custom config file and use `-c </PATH_TO_YOUR_INSTITUTE_CONFIG_FILE/institute_config_file>` in the KiNext launch command.

```bash
nextflow run main.nf -c /PATH_TO_YOUR_INSTITUTE_CONFIG_FILE/institute_config_file -profile test,<singularity>
```

If your institute does not have a referenced config file, you can create one using files from [Nextflow Github page](https://github.com/nf-core/configs/tree/master/docs)

## Running the pipeline

The most basic command for running the pipeline is as follows:

```bash
nextflow run main.nf -profile test,<singularity>
```
This will launch the pipeline with the `test` configuration profile using either docker or singularity.
Note that the pipeline will create the following files in your working directory:

```bash
results # Finished results
.nextflow.log   # Log file from Nextflow
# Other nextflow hidden files, eg. history of pipeline runs and old logs.
```

### Updating the pipeline

When you run the above command, Nextflow automatically pulls the pipeline code from GitHub and stores it as a cached version. When running the pipeline subsequently, the cached version will be used - even if the pipeline has been updated since. To make sure that you're running the latest version of the pipeline, make sure that you regularly update the cached version of the pipeline:

```bash
cd kinext
git pull
```

### Reproducibility

It's a good idea to specify a pipeline version when running the pipeline on your data. This ensures that a specific version of the pipeline code and software are used when you run your pipeline. If the same tag is used (eg. `v1.0.0`), you'll be running the same version of the pipeline, even if there have been changes to the code since.

Go to the KiNext releases page and find the latest version number (eg. `v1.0.0`). Then, configure your local KiNext installation to use the desired version as follows:

```bash
cd kinext
git checkout v1.0.0
```

The pipeline version will be logged in reports when you run the pipeline.

## Required arguments

### `-profile`

Use this parameter to choose a configuration profile. Profiles can give configuration presets for different computing environments. For example: `-profile test,singularity`.
If `-profile` is not specified the pipeline will be run locally and expects all software to be installed and available on the `PATH`.

- `singularity`
  - A generic configuration profile to be used with [Singularity](https://apptainer.org/docs/)
  - Pulls software from DockerHub: [kinext](https://hub.docker.com/r/sebimer/kinext/tags)

Profiles are also available to configure the KiNext workflow and can be combined with execution profiles listed above.
- `test`
  - A profile with a complete configuration that can be applied to a provided test proteome.

- `custom`
  - A profile template to complete according to your dataset and experiment

## Job resources

### Automatic resubmission

Each step in the pipeline has a default set of requirements for number of CPUs, memory and time. For most of the steps in the pipeline, if the job exits with 
an error code of `143` (exceeded requested resources) it will automatically resubmit with higher requests (2 x original, then 3 x original). If it still fails after three times, the pipeline is stopped.

### Custom resource requests

Wherever process-specific requirements are set in the pipeline, the default value can be changed by creating a custom config file. See the files hosted at [`nf-core/configs`](https://github.com/nf-core/configs/tree/master/conf) for examples.

KiNext job resources are defined in **conf/resources.config** file.

## Other command line parameters

## `--outdir`

The output directory where the results will be published.

### `-w/--work-dir`

The temporary directory where intermediate data will be written.

### `--projectName`

Set the name of your project to store the associated result in the corresponding output directory.

### `-name`

Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.

### `-resume`

Specify this when restarting a pipeline if there was a problem during execution and it stopped. Nextflow will used cached results from any pipeline steps where the inputs are the same, continuing from where it stopped.

You can also supply a run name to resume a specific run: `-resume [run-name]`. Use the `nextflow log` command to show previous run names.

**NB:** Single hyphen (core Nextflow option)
