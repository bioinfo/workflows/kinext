# USER GUIDE
If you want to apply **KINEXT** on several proteomes at the same time, you can deposit them here.
The files containing the proteomes you want to analyse must be in **fasta format**
