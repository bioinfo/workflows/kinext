# Introduction

In this file, you will find different HMM (**Hiden Markov Models**) that have been built for the **KiNext** pipeline.

# Construction of the HMM librarie containing an epk and apk model

Here are the different steps that were followed:

<div align="center">
  <img src="assets/HMMmodel_epk_apk_librairie_pipeline.png" height="300">
</div>

## Step 1 and 2
First, a fasta file was constructed from known protein kinase sequences such as those from human, bovine and Drosophila. To do this, these sequences were all downloaded from Kinbase, recovering only the catalytic domains of their protein kinase sequences.
The fact of introducing kinases from different species into this file will allow our HMM (Hidden Markov Model) to be more flexible in its search for new kinases.

## Step 3
The sequences in this file containing reference kinases are then aligned using the [`MAFFT v7.490`](https://mafft.cbrc.jp/alignment/software/manual/manual.html) tool ([Katoh and Standley 2013](https://doi.org/10.1093/nar/gkz342)).
For the alignment of the reference file, the option `--auto` was chosen, which will automatically select one of the alignment methods between L-INS-i, FFT-NS-i and FFT-NS-2 depending on the data size.

## Step 4 and 5
The aligned file output from MAFFT is retrieved by [HMMER v3.3.2](http://eddylab.org/software/hmmer/Userguide.pdf) ([Eddy 2011](https://doi.org/10.1371/journal.pcbi.1002195)). The HMM profile based on the alignment of the reference file is constructed with [`HMMBUILD v3.3.2`](http://eddylab.org/software/hmmer/Userguide.pdf) ([Eddy 2011](https://doi.org/10.1371/journal.pcbi.1002195)).
This HMM library, containing a model for epk searching and a model for apk searching, will be used to find protein sequences homologous to these two models.

# Kinomer project library for the classification of protein kinases according to their sub-family

In order to classify the protein kinases identified by Kinext into sub-families, the **Kinomer library 2** was used. [Kinomer](https://www-compbio-dundee-ac-uk.translate.goog/kinomer?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=fr&_x_tr_pto=sc) is a multilevel HMM library that models these protein kinase groups. **It allows accurate identification of protein kinases and classification to the appropriate kinase group**.

## Application and results

- The large families of sequences were subdivided to generate **multiple HMMs for each family to create the library 2**. The subdivision of large families can increase the recognition accuracy of HMMs by allowing the unique characteristics of each sub-family to be captured more efficiently ([Miranda-Saavedra, D. and Barton, G.J. (2007)](https://doi.org/10.1002/prot.21444).

- Library 2 showed a protein kinase family misclassification rate of zero on characterized kinomes from H. sapiens, M. musculus, D. melanogaster, C. elegans, S. cerevisiae, P. falciparum and D. discoideum ([Miranda-Saavedra, D. and Barton, G.J. (2007)](https://doi.org/10.1002/prot.21444)).

- The results of Diego Miranda-Saavedra and Geoffrey J. Barton indicate that Library 2 is superior not only to BLAST but also to a general HMM of the kinase catalytic domain for database searches. In addition, Library 2 is able to perform automatic classification of protein kinases into families ([Miranda-Saavedra, D. and Barton, G.J. (2007)](https://doi.org/10.1002/prot.21444).

## Converting the library 2 to be compatible with the latest version of HMMER

- Librarie 2 has been renamed: ` all_hmm_libraries.hmm`
- This file was then converted with [`HMMCONVERT v3.3.2`](http://eddylab.org/software/hmmer/Userguide.pdf)` to be compatible with our version of HMMER:

```bash
# Convert all_hmm_libraries.hmm (version 2.0) to hmmer version 3.3.2
hmmconvert all_hmm_libraries.hmm > all_hmm_libraries_convert.hmm
```

# Conclusion

**Thus we have different models and HMM libraries:**
- An HMM model for epk: `epk_model.hmm`
- An HMM model for apk: `apk_model.hmm`
- A HMM library for epk and apk: `epk_apk_model.hmm`
- Kinomer project library 2: `all_hmm_libraries.hmm`
- The converted kinomer project library 2: `all_hmm_libraries_convert.hmm`


# Bibliography

- Rozewicki, John, Songling Li, Karlou Mar Amada, Daron M Standley, and Kazutaka Katoh. “MAFFT-DASH: Integrated Protein Sequence and Structural Alignment.” Nucleic Acids Research, May 7, 2019, gkz342. https://doi.org/10.1093/nar/gkz342.
- Eddy, Sean R. “Accelerated Profile HMM Searches.” PLOS Computational Biology 7, no. 10 (October 20, 2011): e1002195. https://doi.org/10.1371/journal.pcbi.1002195.
- Miranda-Saavedra, D, and Barton, G.J. (2007) Classification and functional annotation of eukaryotic protein kinases. Proteins 68, 893-914. PubMed
- David M. A. Martin; Diego Miranda-Saavedra; Geoffrey J. Barton(2008) Kinomer v. 1.0: a database of systematically classified eukaryotic protein kinases Nucleic Acids Research , doi: 10.1093/nar/gkn834 PubMed

