# USER GUIDE

If you want to test KINEXT on the test proteome (proteins from the Pacific oyster *Crassostrea gigas*: ID 10758), you can use the fasta file which is deposite here.

The file you want to analyse to test the pipeline is in **fasta format**.
