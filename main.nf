#!/usr/bin/env nextflow
/*
========================================================================================
               KINEXT : a Nextflow Workflow for identification, classification and phylogeny of protein kinase
========================================================================================

----------------------------------------------------------------------------------------
*/
nextflow.enable.dsl=2

def helpMessage() {
    // Add to this help message with new command line parameters
    log.info SeBiMERHeader()
    log.info"""

    Usage:

    The typical command for running the pipeline is as follows:

        nextflow run main.nf -c ifremer.config -profile custom [params]

    using following [params] :

Mandatory:
--acc_id	        [str]	Reference assembly accession number.
--data_path		[path]	Absolute path to directory containing raw data.

Other options:
--outdir                [path]	The output directory where the results will be saved.
-w/--workdir            [path]	The temporary directory where intermediate data will be saved.
-name                   [str]	Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.
--projectName           [str]	Name of the project.

        """.stripIndent()
}

// Show help message
if (params.help) {
    helpMessage()
    exit 0
}

// SET UP CONFIGURATION VARIABLES
// Has the run name been specified by the user?
// this has the bonus effect of catching both -name and --name
custom_runName = params.name
if (!(workflow.runName ==~ /[a-z]+_[a-z]+/)) {
    custom_runName = workflow.runName
}

//Copy config files to output directory for each run
paramsfile = file("$baseDir/conf/base.config", checkIfExists: true)
paramsfile.copyTo("$params.outdir/00_pipeline_info/base.config")

// PIPELINE INFO
// Header log info
log.info SeBiMERHeader()
def summary = [:]
if (workflow.revision) summary['Pipeline Release'] = workflow.revision
summary['Run Name'] = custom_runName ?: workflow.runName
summary['Project Name'] = params.projectName
summary['Output dir'] = params.outdir
summary['Launch dir'] = workflow.launchDir
summary['Working dir'] = workflow.workDir
summary['Script dir'] = workflow.projectDir
summary['User'] = workflow.userName
summary['Execution profile'] = workflow.profile

log.info summary.collect { k,v -> "${k.padRight(18)}: $v" }.join("\n")
log.info "-\033[91m--------------------------------------------------\033[0m-"

// Check hostnames against configured profiles
checkHostname()

/*
 * CHECK AND SET UP WORKFLOW VARIABLES
 */

/*
 * IMPORTING MODULES
 */
include { HMMsearch }			from './modules/hmm_search.nf'
include { AssignedPK }                   from './modules/assigned_epk_apk.nf'
include { HMMsearch as HMMsearchFamilies }                   from './modules/hmm_search.nf'
include { ClassifyFamilies }                   from './modules/classify_families.nf'
include { HMMalign as HMMalignEPK }                   from './modules/hmm_align.nf'
include { HMMalign as HMMalignAPK }                   from './modules/hmm_align.nf'
include { Iqtree as IqtreeEPK }                   from './modules/iqtree.nf'
include { Iqtree as IqtreeAPK }                   from './modules/iqtree.nf'
include { SummaryOUT }                   from './modules/summary_output.nf'


/*
 * CREATE INPUT CHANNELS
 */
proteome = Channel.fromPath("${params.proteome}")
hmm_model = Channel.fromPath("${params.epk_apk_hmm_model}")
hmm_model_families = Channel.fromPath("${params.families_hmm_model}")
hmm_model_epk = Channel.fromPath("${params.epk_hmm_model}")
hmm_model_apk = Channel.fromPath("${params.apk_hmm_model}")
name_epk_apk = Channel.of("${params.name_epk_apk}")
name_families = Channel.of("${params.name_families}")
name_epk = Channel.of("${params.name_epk}")
name_apk = Channel.of("${params.name_apk}")

/*
 * RUN MAIN WORKFLOW
 */
workflow {
    proteome.view()

    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // HMMSEARCH KINASE IN PROTEOME	
    proteome
        .combine(hmm_model)
        .combine(name_epk_apk)
        .set { proteome_hmm_model }
    // The resulting channel contains 3 elements
    // The path of the proteome
    // The HMM model used
    // The name of the step (for the output file name)

    // PROCESS
    // --------------------------------
    HMMsearch(proteome_hmm_model)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // ASSIGNED EPK AND APK USING THEIR EVALUE (python script)

    // PROCESS
    // --------------------------------    
    AssignedPK(HMMsearch.out.hmm_search_output)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // HMMSEARCH FAMILIES IN KINOME

    AssignedPK.out.kinome_output
        .combine(hmm_model_families)
        .combine(name_families)
        .set { kinome_hmm_model }
    //kinome_hmm_model.view()
    // The resulting channel contains 3 elements
    // Kinome identified by HMMsearch
    // The name of the step, here it's the families_identification
    // The hmm model or library used


    // PROCESS
    // --------------------------------
    HMMsearchFamilies(kinome_hmm_model)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // CLASSIFY KINASES INTO FAMILIES
   

    // split proteome channel 
    proteome
        .map { it -> [ it.baseName, it ] }
        .set { proteome_ID_ch }
    //proteome_ID_ch.view()
    // the resulting channel contains 2 elements
    // proteome name
    // proteome path 


    HMMsearchFamilies.out.hmm_search_output
        .map { it -> [ it[0].baseName.split('_')[0..-2].join('_'), it[0], it[1] ] }
        .set { kinome_ID_ch }
    //kinome_ID_ch.view()
    // The resulting channel contains 3 elements
    // Kinome name ( equivalent to the proteome name)
    // Kinome path
    // HMM search families .out

     
    proteome_ID_ch.join(kinome_ID_ch)
        .set { proteome_kinome_ch }
    //proteome_kinome_ch.view()
    // the resulting channel contains 4 elements
    // proteome name
    // proteome path
    // kinome path
    // HMM search families .out


    // PROCESS
    // --------------------------------
    ClassifyFamilies(proteome_kinome_ch)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // CREATION OF THE FINAL CVS OUTPUT


    HMMsearch.out.hmm_search_output
        .map { it -> [ it[0].baseName, it[1] ] }
        .set { first_hmmsearch_ID_ch }
    //first_hmmsearch_ID_ch.view()

    // the resulting channel contains 2 elements
    // proteome name
    // HMM search epk apk .out


    first_hmmsearch_ID_ch.join(ClassifyFamilies.out.classify_kinome_ch)
        .set { hmmepkapk_hmmfamilies_ch }
    hmmepkapk_hmmfamilies_ch.view()

     
    //proteome_hmmoutputs_kinome_ch.view()

    // PROCESS
    // --------------------------------
    SummaryOUT(hmmepkapk_hmmfamilies_ch)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    //HMMALIGN EPK KINOME ON THE EPK MODEL

    ClassifyFamilies.out.classify_epk_ch
        .combine(hmm_model_epk)
	.combine(name_epk)
        .set { proteome_hmmepk_ch }
    //proteome_hmmepk_ch.view()

    // PROCESS
    // --------------------------------
    HMMalignEPK(proteome_hmmepk_ch)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    //HMMALIGN APK KINOME ON THE APK MODEL

    ClassifyFamilies.out.classify_apk_ch
        .combine(hmm_model_apk)
        .combine(name_apk)
        .set { proteome_hmmapk_ch }
    //proteome_hmmapk_ch.view()

    // PROCESS
    // --------------------------------
    HMMalignAPK(proteome_hmmapk_ch)
    // --------------------------------




    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // BUILDING THE TREE FILE FOR THE EPK PHYLOGENY

    HMMalignEPK.out.hmmalign_ch
        .combine(name_epk)
        .set { hmmalign_epk_out_ch }
    //hmmalign_epk_out_ch.view()

    // PROCESS
    // --------------------------------
    IqtreeEPK(hmmalign_epk_out_ch)
    // --------------------------------



    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // BUILDING THE TREE FILE FOR THE APK PHYLOGENY

    HMMalignAPK.out.hmmalign_ch
        .combine(name_apk)
        .set { hmmalign_apk_out_ch }
    //hmmalign_apk_out_ch.view()

    // PROCESS
    // --------------------------------
    IqtreeAPK(hmmalign_apk_out_ch)
    // --------------------------------


}



/*
 * Completion notification
 */

workflow.onComplete {
    c_green = params.monochrome_logs ? '' : "\033[0;32m";
    c_purple = params.monochrome_logs ? '' : "\033[0;35m";
    c_red = params.monochrome_logs ? '' : "\033[0;31m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    
    def msg = """\
        Pipeline execution summary
        ---------------------------
        Completed at: ${workflow.complete}
        Duration    : ${workflow.duration}
        Success     : ${workflow.success}
        workDir     : ${workflow.workDir}
        exit status : ${workflow.exitStatus}
        """
    .stripIndent()
    if (workflow.success) {
        log.info "-${c_purple}[Workflow info]${c_green} KiNext workflow completed successfully${c_reset}-"
    } else {
        checkHostname()
        log.info "-${c_purple}[Workflow info]${c_red} KiNext workflow completed with errors${c_reset}-"
    }
}

/*
 * Other functions
 */

def SeBiMERHeader() {
    // Log colors ANSI codes
    c_red = params.monochrome_logs ? '' : "\033[0;91m";
    c_blue = params.monochrome_logs ? '' : "\033[1;94m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    c_yellow = params.monochrome_logs ? '' : "\033[1;93m";
    c_Ipurple = params.monochrome_logs ? '' : "\033[0;95m" ;

    return """    -${c_red}--------------------------------------------------${c_reset}-

    ${c_blue}  ╔═══╗────╔══╗───╔═╗╔═╗╔═══╗╔═══╗    ${c_blue}
    ${c_blue}  ║╔═╗║────║╔╗║───║║╚╝║║║╔══╝║╔═╗║    ${c_blue}
    ${c_blue}  ║╚══╗╔══╗║╚╝╚╗╔╗║╔╗╔╗║║╚══╗║╚═╝║    ${c_blue}
    ${c_blue}  ╚══╗║║║═╣║╔═╗║╠╣║║║║║║║╔══╝║╔╗╔╝    ${c_blue}
    ${c_blue}  ║╚═╝║║║═╣║╚═╝║║║║║║║║║║╚══╗║║║╚╗    ${c_blue}
    ${c_blue}  ╚═══╝╚══╝╚═══╝╚╝╚╝╚╝╚╝╚═══╝╚╝╚═╝    ${c_blue}
    ${c_yellow}  KiNext workflow (version ${workflow.manifest.version})${c_reset}
                                            ${c_reset}
    ${c_Ipurple}  Homepage: ${workflow.manifest.homePage}${c_reset}
    -${c_red}--------------------------------------------------${c_reset}-
    """.stripIndent()
}

def checkHostname() {
    def c_reset = params.monochrome_logs ? '' : "\033[0m"
    def c_white = params.monochrome_logs ? '' : "\033[0;37m"
    def c_red = params.monochrome_logs ? '' : "\033[1;91m"
    def c_yellow_bold = params.monochrome_logs ? '' : "\033[1;93m"
    if (params.hostnames) {
        def hostname = "hostname".execute().text.trim()
        params.hostnames.each { prof, hnames ->
            hnames.each { hname ->
                if (hostname.contains(hname) && !workflow.profile.contains(prof)) {
                    log.error "====================================================\n" +
                            "  ${c_red}WARNING!${c_reset} You are running with `-profile $workflow.profile`\n" +
                            "  but your machine hostname is ${c_white}'$hostname'${c_reset}\n" +
                            "  ${c_yellow_bold}It's highly recommended that you use `-profile $prof${c_reset}`\n" +
                            "============================================================"
                }
            }
        }
    }
}

