process AssignedPK {

    label 'kinext'
    publishDir "${params.resultdir}/assigned_epk_apk",         mode: 'copy', pattern: '*.fasta'
    publishDir "${params.resultdir}/logs/assigned_epk_apk",    mode: 'copy', pattern: '*.log'
    publishDir "${params.resultdir}/unsorted/assigned_epk_apk",    mode: 'copy', pattern: '*'

    input:
        tuple path(proteome), path(hmm_search_output)

    output:
        tuple path("*_kinome.fasta"), path("${hmm_search_output}"), emit: output_assigned_pk
        path("*_epk.fasta"), emit: epk_output
        path("*_apk.fasta"), emit: apk_output
        path("*_kinome.fasta"), emit: kinome_output
        path("*_assignedPK.log")

    script:
    """
assigned_epk_apk.py -hmm ${hmm_search_output} -p ${proteome} -k ${proteome.baseName}_kinome.fasta >& ${proteome.baseName}_assignedPK.log 2>&1
mv apk.fasta ${proteome.baseName}_apk.fasta
mv epk.fasta ${proteome.baseName}_epk.fasta
    """
}

