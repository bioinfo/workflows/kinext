process SummaryOUT {

    label 'kinext'
    publishDir "${params.resultdir}/summary_output",         mode: 'copy', pattern: '*.csv'
    publishDir "${params.resultdir}/unsorted/summary_output",         mode: 'copy', pattern: '*'
    publishDir "${params.resultdir}/logs/summary_output",    mode: 'copy', pattern: '*.log'

    input:
        tuple val(genome_id), path(HMMsearch), path(proteome), path(HMMsearchFamilies), path(whole_kinome), path(annotated_kinome)

    output:
        path("*.csv"), emit: summary_output
    
    script:
    """
echo HMMsearch: ${HMMsearch}
echo HMMsearchFamilies: ${HMMsearchFamilies}
echo whole_kinome: ${whole_kinome}
echo proteome: ${proteome}
summary_output.py -H1 ${HMMsearch} -H2 ${HMMsearchFamilies} -W ${whole_kinome} -A ${annotated_kinome} >& ${proteome.baseName}_summary_output.log 2>&1
mv 1_summary_table_output.csv ${proteome.baseName}_1_summary_table_output.csv
mv 2_summary_table_output.csv ${proteome.baseName}_2_summary_table_output.csv
    """
}
