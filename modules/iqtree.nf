process Iqtree {

    label 'iqtree2'
    publishDir "${params.resultdir}/iqtree",         mode: 'copy', pattern: '*'
    publishDir "${params.resultdir}/logs/iqtree",    mode: 'copy', pattern: '*.log'
    publishDir "${params.resultdir}/unsorted/iqtree",    mode: 'copy', pattern: '*'

    input:
        tuple val(genome_id), path(proteome), path(hmmalign_output), val(name)

    output:
        path("*.treefile"), emit: iqtree_output
        path("*.log")

    script:
    """
    iqtree2 -nt ${NCPUS} -s ${hmmalign_output} -st AA -bb 1000 -alrt 1000 -m MFP > ${proteome.baseName}_${name}_iqtree.log 2>&1

    """
}     
