process HMMalign {

    label 'hmmer3'
    publishDir "${params.resultdir}/hmmalign",         mode: 'copy', pattern: '*.phy'
    publishDir "${params.resultdir}/logs/hmmalign",    mode: 'copy', pattern: '*.log'
    publishDir "${params.resultdir}/unsorted/hmmalign",    mode: 'copy', pattern: '*'

    input:
        tuple val(genome_id), path(proteome), path(fasta), path(hmm_model), val(name)

    output:
        path("*.phy"), emit: hmmalign_output
        path("*.log")
	tuple val("${genome_id}"), path("${proteome}"), path("*.phy"), emit: hmmalign_ch

    script:
    """
    hmmalign --trim --outformat clustal -o ${proteome.baseName}_${name}_hmmalign.phy ${hmm_model} ${fasta} >& ${proteome.baseName}_${name}_phylip_hmmer_align.log 2>&1

    """
}
