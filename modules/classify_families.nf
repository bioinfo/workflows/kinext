process ClassifyFamilies {

    label 'kinext'
    publishDir "${params.resultdir}/classify_families",         mode: 'copy', pattern: '*.fasta'
    publishDir "${params.resultdir}/logs/classify_families",    mode: 'copy', pattern: '*.log'
    publishDir "${params.resultdir}/unsorted/classify_families",    mode: 'copy', pattern: '*'

    input:
        tuple val(genome_id), path(proteome), path(kinome), path(hmm_search_output)

    output:
        path("*_annotated_epk.fasta"), emit: epk_annot_output
        path("*_annotated_apk.fasta"), emit: apk_annot_output
        path("*_annotated_kinome.fasta"), emit: kinome_annot_output
        path("*_kinase_no_annot.fasta"), emit: kinase_no_annot_output
        path("*_whole_kinome.fasta"), emit: whole_kinome_output
        path("*_classify_families.log")
        tuple val("${genome_id}"), path("${proteome}"), path("*_annotated_epk.fasta"), emit: classify_epk_ch
        tuple val("${genome_id}"), path("${proteome}"), path("*_annotated_apk.fasta"), emit: classify_apk_ch
        tuple val("${genome_id}"), path("${proteome}"), path("${hmm_search_output}"), path("*_whole_kinome.fasta"), path("*_annotated_kinome.fasta"), emit: classify_kinome_ch
    script:
    """
classify_families.py -hmm ${hmm_search_output} -k ${kinome} >& ${proteome.baseName}_classify_families.log 2>&1
mv epk_annot.fasta ${proteome.baseName}_annotated_epk.fasta
mv apk_annot.fasta ${proteome.baseName}_annotated_apk.fasta
mv kinome_annot.fasta ${proteome.baseName}_annotated_kinome.fasta
mv kinase_no_annot.fasta ${proteome.baseName}_kinase_no_annot.fasta
mv whole_kinome.fasta ${proteome.baseName}_whole_kinome.fasta
    """
}
